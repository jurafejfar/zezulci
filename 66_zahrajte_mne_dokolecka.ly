% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI = {
         a'8 a' d''4 d''8( e''8) |
         f''8 d'' e''4 a' |
         a'8 h' c''4 c'' |
         h'8 a' g'4 f' |
\repeat volta 2{
         f'8 g' a'4 h' |
         c''8 h' a'2 |
         g'8 h' a'4 g' |
         f'8 e' d'2 }
}

MvmntIVoiceILyricsVerseI = \lyricmode {
	Za -- hraj -- te mně do -- ko -- ľeč -- ka,
	je tu mo -- ja ga -- ľá -- neč -- ka.
\repeat volta 2{
	Za -- hraj -- te mně do -- ko -- la,
	je tu ga -- ľán -- ka mo -- ja.
}}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
    d2.:m
    d4:m a2:m
    a4:m c2
    c2 f4
    f2.
    g4 f2
    g2.
    d2.:m
}


MvmntIVoiceITimeSig = \time 3/4 
MvmntIVoiceIKeySig = \key d \minor
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \transpose d c \MvmntI}
