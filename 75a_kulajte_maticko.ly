% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI =  {
	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
\repeat volta 2 {
         d'8 fis' a' a' a' d'' 
         h' h' a'4 fis'
}
         g'8 g' g' fis' e' d' 
         cis' d' e'4 fis' 
         h'8 h' h' a' h' a' 
         g' fis' e'4 d' \bar "|."
}

MvmntIVoiceILyricsVerseI = \lyricmode {
\repeat volta 2 {
	Kú -- lej -- te, ma -- tič -- ko, ko -- šu -- ľen -- ku,
}
	a já si po -- je -- du pro mi -- ľen -- ku,
	a já si po -- je -- du pro mi -- ľen -- ku.
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
    d2.
    g4 d2
    g2.
    a2 d4
    g2.
    a2 d4
}



MvmntIVoiceITimeSig = \time 3/4 
MvmntIVoiceIKeySig = \key d \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \MvmntI}
