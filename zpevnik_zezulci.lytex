% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\documentclass[12pt]{article} %[14pt]{extreport}
\usepackage[ a5paper, top=2cm, bottom=2cm, left=1cm, right=1cm ]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[czech]{babel}
\usepackage{graphicx}
\usepackage[colorlinks=true, linkcolor=blue, urlcolor=blue, unicode]{hyperref}

\begin{document}

\begin{titlepage}

\begin{center}

% Upper part of the page
\includegraphics[width=5cm]{../zezulka_titul_draw.png}

% Title

\rule{\linewidth}{0.5mm}\\[0.4cm]
{ \huge \bfseries Zezulci}\\[0.4cm]
\rule{\linewidth}{0.5mm}\\[0.4cm]

\Large z nahrávek Zezulkovy muziky\\[0.5cm]
\vfill
\large \today, svatá Anna \\[0.5cm]

\end{center}

\end{titlepage}

\pagestyle{empty}
    Písně byly nahrány roku 1955 ve Valašské Bystřici\footnote{MAJEROVÁ, Marie. \textit{Lidová písňová tradice ve Valašské Bystřici} [online]. Str. 37. Brno, 2012. Dostupné z: \href{https://is.muni.cz/th/x7c38/}{https://is.muni.cz/th/x7c38/}. PdF MU.}.
    Do not přepsal a zpěvník vysázel\footnote{Vysázeno v programu \href{http://lilypond.org/index.cs.html}{http://lilypond.org/index.cs.html}} Jiří Fejfar.
    Přepis do not je kompromisem mezi věrností nahrávce a jednoduchostí – možností ze zpěvníku přímo hrát.
    
    Poděkování za pomoc s dokončením zpěvníku patří Petře Dostálové, Janu Adamci ml. a Štěpánu Krhutovi.
    Harmonizace je v nahrávkách pouze místy naznačena vícehlasy.
    Zapsané akordy ve zpěvníku vycházejí ze současného způsobu doprovodu, zejména muziky Troják\footnote{\href{https://www.google.com/search?q=folklorni+soubor+trojak}{https://www.google.com/search?q=folklorni+soubor+trojak}} z Valašské Bystřice. 
    
    \vspace{5mm}
    
    Na stránkách \href{https://jurafejfar.gitlab.io/zezulci/}{https://jurafejfar.gitlab.io/zezulci/} naleznete nejnovější verzi zpěvníku.
    Připomínky, přání a stížnosti můžete zasílat na email \href{mailto:jfejfar@seznam.cz}{jfejfar@seznam.cz}.

    \vfill
    Tento zpěvník podléhá licenci Creative Commons Uveďte původ-Zachovejte licenci 4.0 Mezinárodní. Pro zobrazení licenčních podmínek navštivte 
    \href{https://creativecommons.org/licenses/by-sa/4.0/deed.cs}{https://creativecommons.org/licenses/by-sa/4.0/deed.cs}.

\begin{center}
    \includegraphics[width=2cm]{../by-sa.pdf}
\end{center}

\newcommand{\betweenLilyPondSystem}[1]{\vspace{3mm}\linebreak}
\clearpage
\pagestyle{empty}
\tableofcontents
\clearpage
\pagestyle{plain}
	\input{01_moja_zena.lytex}
	\input{02_zabolela_mna_hlava.lytex}
	\input{03_tisnavskych_pacholku.lytex}
	\input{04_tece_voda.lytex}
	\input{05_aj_malo_jsem_sa_napil.lytex}
	\input{07_a_ty_jedlicko.lytex}
	\input{08_a_horecky_kopecky.lytex}
	\input{09_aj_lucina_lucina.lytex}
	\input{10_kde_si_synecku.lytex}
	\input{11_aj_hora_hora.lytex}
	\input{12_a_ci_su_to_kravy.lytex}
	\input{13_kdyz_jsem_byval_mladeneckem.lytex}
	\input{14_vy_hajnici_hory_hajte.lytex}
	\input{15_zazpivaj_kohutku.lytex}
	\input{16_aj_u_susedu.lytex}
	\input{17_v_sirem_poli.lytex}
	\input{18_aj_lasko_lasko.lytex}
	\input{19_od_radhosta_svita.lytex}
	\input{22_ten_bystricky_mostek.lytex}
	\input{23_tobe_dobre_muj_synecku.lytex}
	\input{24_sedlak_v_lese.lytex}
	\input{25_ty_zeleny_vrsku.lytex}
	\input{26_tece_voda_z_javora.lytex}
	\input{28_pivo_pivo.lytex}
	\input{29_kdyz_jsem_ja_sel_od_milenky.lytex}
	\input{30_vlastovicka_svehliva.lytex}
	\input{31_a_ja_jsem_nebyl_doma.lytex}
	\input{32_pasel_jano_kone.lytex}
	\input{33_uz_je_mala_chvilka_do_dne.lytex}
	\input{35_zpivajte_bystricti_kohuti.lytex}
	\input{36_pod_horami_jatelina.lytex}
	\input{37_kamarade_bratre_muj.lytex}
	\input{38_marianko_safarova.lytex}
	\input{39_malenacek_z_lubenek.lytex}
	\input{41_tece_voda_skokem.lytex}
	\input{43_aj_muselas_me_pocarovat.lytex}
	\input{44_neni_kazde_devce_verne.lytex}

	\input{45_jedu_chlapeci.lytex}
	\input{52_vim_ja_jedno_okenko.lytex}
	\input{54_slavicek_ptacek_malicky.lytex}
	\input{56_zalo_devce.lytex}
	\input{57_kdyz_jsem_byla.lytex}
	\input{58_zahrajte_mne_huslicky.lytex}
	\input{61_a_ta_mala_zahradecka.lytex}
	\input{63_a_mel_jsem_ja_sakulensku.lytex}
	\input{64_kdyby_me_dal_panbuh_dostat.lytex}
	\input{66_zahrajte_mne_dokolecka.lytex}
	\input{75a_kulajte_maticko.lytex}
	\input{75b_a_ideme_ideme.lytex}
	\input{75c_pochvalen_bud.lytex}
\end{document}
