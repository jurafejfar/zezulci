% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI = {
\repeat volta 2 {
        h8 h e' e' |
         gis' gis' fis' e' |
         dis' e' fis' gis' |
         a' a' gis'4 |
%5
         fis'2  }
         h'4 h'8 a'8 |
         gis'4 fis'8 e' |
         dis'4 e' |
         fis'2 |
%10
         a'4 cis''8 cis'' |
         h'4 h'8 a' |
         gis'4 fis' |
         e'2 \bar "|."
}

MvmntIVoiceILyricsVerseI = \lyricmode { 
	\repeat volta 2 {Už je ma -- lá chvil -- ka do dně,
        poď má mi -- lá, vy -- pro -- voď ně.}
        A už je ma -- ľič -- ká chvil -- čič -- ka
        do te -- ho bí -- lé -- ho rá -- níč -- ka.
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
    e2
    e2
    h2
    a4 e4
    h2
    e2
    e2
    h4 e4
    h2
    a2
    e2
    e4 h4
    e2
}


MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key e \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \transpose e g \MvmntI}
