% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI =  {
         h'8 h' h' cis'' |
         d'' d'' cis'' h' |
         cis'' a' a' a' |
         gis' a' h' h' |
%5
\repeat volta 2 {         e' fis' g'?8 h' |
         a' g' fis'4 |
         e'8 fis' g' e' |
         dis' fis' e'4 |}
}

MvmntIVoiceILyricsVerseI = \lyricmode { 
	Dyž sem já šél od mi -- len -- ky, břin -- ka -- ly mně pod -- ko -- věn -- ky,
	\repeat volta 2 {břin -- ka -- ly mně, břin -- ka -- ly, čer -- né o -- či pla -- ka -- ly.}
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
  e2
  e2
  a2
  e2
  e2:m
  e4:m h4
  e2:m
  h4 e4:m
}


MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key e \minor
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \MvmntI}
