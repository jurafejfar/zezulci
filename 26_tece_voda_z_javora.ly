% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI =  {
         f'8 f' g' g' |
         a' f' g'4 |
         c'' d''8 h' |
         d'' c'' h' a' |
%5
         g' f' g'( b') |
         a'4 g' |
         f'2 \bar "|."
}

MvmntIVoiceILyricsVerseI = \lyricmode {
	Te -- če vo -- da z_ja -- vo -- ra, z_ja -- vo -- ra,
	méj An -- dul -- ce do dvo -- ra, do dvo -- ra.
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
  f4 c4
  f4 c4
  f4 g4
  g2
  c8 f8 c8 c8:7
  f4 c4
  f2
}



MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key d \minor
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \transpose f g \MvmntI}
