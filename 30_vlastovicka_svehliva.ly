% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI = {
         c''8 h' a' g' |
         c''4 d'' |
         e''2 |
         c''8 h' a' g' |
%5
         c''4 d'' |
         e''2 |
         e''8 d'' cis''4 |
         a'2 |
         d''8 e'' d''4 |
%10
         g'2  
         \repeat volta 2 { c''8 h' a' g' |
         c'' d'' e'' e'' |
         c'' h' a' g' |
         c'' d'' e'' e'' |
%15
         e'' d'' cis''4 |
         a'2 }
}

MvmntIVoiceILyricsVerseI = \lyricmode { 
	Vlaš -- to -- vič -- ka šveh -- lí -- vá při -- mod -- ra -- lé pe -- ří má,
	rá -- no vstá -- vá, nás bu -- dí -- vá. 
	\repeat volta 2 {Vlaš -- to -- věn -- ka švi -- dli, švi -- dli,
	na hú -- ře má své o -- byd -- lí, tam pře -- bý -- vá.}
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
  c2
  c4 g4
  c2
  c2
  c4 g4
  c2
  a2
  a2
  d2
  g2
  c4. g8
  c2
  c4. g8
  c2
  a2
  a2
}

MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key c \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \transpose c' b \MvmntI}
