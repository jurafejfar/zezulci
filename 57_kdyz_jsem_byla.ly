% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI =  {
  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
         c'8 e' g'4 g' |
         b'8 a' g' f' e' d' |
         c' e' g'4 g' |
         b'8 a' g' a' g' f' |
%5
         e' d' c'4 b |
         d'8 d' d' e' f' e' |
         e' d' c'4 b |
         d'8 d' d' e' f' g' |
         c'2. \bar "|."
}

MvmntIVoiceILyricsVerseI = \lyricmode {
	Dyž sem by -- la u svéj mam -- ky v_do -- mě,
	dyž sem by -- la u svéj mam -- ky v_do -- mě,
	šest mlá -- den -- ců cho -- dí -- va -- lo ke mně, 
	šest mlá -- den -- ců cho -- dí -- va -- lo ke mně, 
	héj.
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
    c2.
    c2.
    c2.
    c2.
    c2 b4
    b2.
    c2 b4
    b2 g4
    c2.
}

MvmntIVoiceITimeSig = \time 3/4 
MvmntIVoiceIKeySig = \key c \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \MvmntI}
