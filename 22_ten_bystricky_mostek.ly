% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI =  {
         c'8 d' e' f' |
         g'4 f' |
         a'8 a' a' c'' |
         b'4 a' |
%5
         c''8 a' c'' f'' |
         d''4 c'' |
         c''8 b' a'4 |
         g'2 |
         b'8 a' g'4 |
%10
         f'2 \bar "|."}

MvmntIVoiceILyricsVerseI = \lyricmode {
		Ten bys -- třic -- ký mos -- tek,
		ten bys -- třic -- ký mos -- tek,
		ten bys -- třic -- ký mos -- tek
		pro -- hý -- bá sa, "aj pro" -- hý -- bá sa.
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
 f2
 c4 f4
 f2
 c4:7 f4
 f2
 b4 f4
 f2
 c2
 c2:7 
 f2
}



MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key f \major
MvmntIVoiceIClef = \clef treble
MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \MvmntI}
