% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI = {
         e'8 gis' h' h' |
         h' h' a' gis' |
         h'4 a'8 a' |
         a' gis' fis' e' |
%5
\repeat volta 2{
         fis'4 h |
         e' fis' |
         gis'8 ais' h' h' |
         ais' h' ais' gis' |
         fis'4 e'  |
         }
}

MvmntIVoiceILyricsVerseI = \lyricmode {
	Ma -- ri -- án -- ko Ša -- fá -- řo -- va, máš -- li ty hu -- sy do -- ma,
	\repeat volta 2{máš -- li, máš -- li, či o -- ny ti za po -- tů -- ček zaš -- ly.}
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
    e2
    e2
    a2
    e2
    h2
    e4 h4
    e2
    fis2
    h4 e4
}

MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key e \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \transpose e g \MvmntI}
