% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI = {
         e'4 fis'8 gis' |
         a'4 h'8 a' |
         gis'4 fis' |
         e'2 |
%5
         h'4 cis''8 dis'' |
         e''4 fis''8 e'' |
         dis''4 cis'' |
         h'2 |
\repeat volta 2 {
         h'8 h' e'' e'' |
%10
         e'' dis'' cis''4 |
         a'8 a' a' a' |
         a' cis'' h'4 |
         e' e'8 gis' |
         h'4 h'8 a' |
%15
         gis'4 fis' |
         e'2 
}
}

MvmntIVoiceILyricsVerseI = \lyricmode { 
	Zpí -- vaj -- te bys -- třič -- tí ko -- hú -- ti,
        ať se mé sr -- déč -- ko ne -- rmú -- tí.
        \repeat volta 2 { Za -- zpí -- vaj -- te jak je -- den,
        za -- zpí -- vaj -- te jak je -- den,
	ke -- rá je u -- přím -- ná vy -- jde ven.}
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
    e2
    a2
    e4 h4
    e2
    h2
    e2
    fis2
    h2
    e2
    e4 a4
    a2
    a4 e4
    e2
    e2
    e4 h4
    e2
}


MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key e \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \transpose e d \MvmntI}
