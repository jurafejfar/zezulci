% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI =  {
         d''4 d''8 d'' |
         cis''4. h'8 |
         a'4 g' |
         h' g'8 g' |
%5
         a'4. h'8 |
         cis''4 a' |
         d'' d''8 d'' |
         cis''4. h'8 |
         a'4 g' |
%10
         h'8 h'4 g'8 |
         a'4. g'8 |
         fis'4 g' \bar "|."
}

MvmntIVoiceILyricsVerseI = \lyricmode {
	Aj má -- lo sem sa na -- píl 
	a moc mám na -- psa -- né -- ho. 
	Aj byl bych sko -- rem pro -- píl, 
	ko -- níč -- ka sed -- lo -- vé -- ho.
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
 g2
 a2
 d4 g4
 g2
 d2
 a2
 g2
 a2
 d4 g4
 g2
 d2
 d4 g4
}

MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key g \major
MvmntIVoiceIClef = \clef treble
MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \MvmntI}
