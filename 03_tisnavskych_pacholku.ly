% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI = {
\repeat volta 2{
         a'8 gis' a' h' |
         a'4 g' |
         g' \tuplet 3/2 {fis'8 ( e' d')}  |
         e'2 |}
%5
         g'4 f'?8 d' |
         e'4 c' |
         g' f'8 d' |
         e'4 c' |
         a8 h cis' d' |
%10
         f' d' cis'4 |
         d'2 \bar "|."
}

MvmntIVoiceILyricsVerseI = \lyricmode { 
	\repeat volta 2{
	Tís -- ňav -- ských pa -- chol -- ků 
	do -- ma_ne -- ní,} 
	a o -- ni jé -- li do čer -- nej ho -- ry 
	na ka -- me -- ní, na ka -- me -- ní. 
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t

%chord symbols follow
 a2
 a4 g4
 g2
 a2
 g2
 c2
 g2
 c2
 a2
 d4:m a4
 d2:m 
}

MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key d \minor
MvmntIVoiceIClef = \clef treble
MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \transpose d' g' \MvmntI}
