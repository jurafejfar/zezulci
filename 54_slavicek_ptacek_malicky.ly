% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI = {
\repeat volta 2 {
         f'4 g' a' |
         b'2 f'4 |
         a'2 g'4 |
         es'2 f'4 |
         a'2 g'4 |
         f'2 es'4 |
         d'2.  |
}
         r2 f'4 |
         d''2 c''4 |
         b'2 a'4 |
         g'2. |
         r2 g'4 |
         c''2 b'4 |
         a'2 g'4 |
         f'2. |
         f'4 g' a' |
         b'2 f'4 |
         a'2 g'4 |
         es'2 f'4 |
         a'2 g'4 |
         f'2 es'4 |
         d'2.  |
}

MvmntIVoiceILyricsVerse = \lyricmode {
	Sla -- ví -- ček, ptá -- ček ma -- lič -- ký,
	jak on li -- bě pě -- je.
}

MvmntIVoiceILyricsVerseI = \lyricmode {
	Všec -- ky pa -- nen -- ky z_trá -- vy jdú,
	on se na ně smě -- je.
	Jen ta mo -- je ne -- jde,
	jen ta mo -- je ne -- jde.
	O -- na se vel -- mi po -- ža -- la,
	snáď jí krev o -- dej -- de.
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
 f2.
 b2.
 es2.
 es2.
 f2.
 f2.
 b2.
%------------------
 b2.
 b2.
 b2.
 es2.
 es2.
 f2.
 f2.
 b2.
 f2.
 b2.
 es2.
 es2.
 f2.
 f2.
 b2.
}



MvmntIVoiceITimeSig = \time 3/4 
MvmntIVoiceIKeySig = \key b \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << 
	\new Voice = "melody" { \MvmntIVoiceIMusic } 
	\new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerse } 
	\new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } 
>>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \MvmntI}
