% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI = {
  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
         c''4 f'' d'' |
         c''2 a'4 |
         g'8 a' b'4 a' |
         c'' f'' d'' |
%5
         c''2 a'4 |
         g'8 b' a'2 |
\repeat volta 2{
         d''8 d'' b'4 d''4 |
         c''8 c'' a'4 f' |}
%10

\alternative {{
         g'8( a') b' a' g' e'
        f' a' c''2 |}
        {
         g'8( a') b' a' g' f'
         e' g' f'2  \bar "|."}}
}

MvmntIVoiceILyricsVerseI = \lyricmode {
	Aj mu -- sé -- las mně po -- ča -- ro -- vať, 
	aj mu -- sé -- las mně po -- dě -- lať,
\repeat volta 2{
	mu -- se -- las mně do ví -- neč -- ka
}\alternative {{
	aj čer -- ve -- né -- ho 
	co -- si dať
}{
	aj čer -- ve -- né -- ho 
	co -- si dať.
}}
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
    f2 b4
    f2.
    c2 f4
    f2 b4
    f2.
    c4 f2
    b2.
    f2.
    c2.
    f2.
  \set chordChanges = ##f
    c2.
  \set chordChanges = ##t
    c4 f2
}



MvmntIVoiceITimeSig = \time 3/4
MvmntIVoiceIKeySig = \key f \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \MvmntI}
