% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI =  {
         \repeat volta 2{
         h'4 h'8 d'' |
         cis''4 h' |
         a' a'8 g' |
         fis'4 e' |}
%5
         \repeat volta 2{
         fis' d' |
         g' g'8 h' |
         a'4 a'8 g' |
         fis'4 e' }
}

MvmntIVoiceILyricsVerseI = \lyricmode { 
	\repeat volta 2{Aj ho -- ra, ho -- ra, ze -- ľe -- ná ho -- ra,}
	\repeat volta 2{do téj ho -- reč -- ky co -- si mňa vo -- lá.} 
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
 g2
 a4 g4 
 a4.:m e8:m
 h4 e4:m
%5
 d2
 g2
 a4.:m e8:m
 h4 e4:m 
}

MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key g \major
MvmntIVoiceIClef = \clef treble
MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \MvmntI}
