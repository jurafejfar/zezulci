% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI = {
         b8 b c' d' |
         es' es' f' es' |
         d' d' c'4 |
         b2 |
%5
         f'8 f' g' a' |
         b' b' c'' b' |
         a' a' g'4 |
         f'2 |
\repeat volta 2{
         f'8 b' b' b' |
%10
         b' a' g'4 |
         g'8 f' es' es' |
         g' g' f'4 |
         b8 b c' d' |
         es' g' f' es' |
%15
         d' d' c'4 |
         b2}
}

MvmntIVoiceILyricsVerseI = \lyricmode {
	Ža -- lo děv -- če, ža -- lo trá -- vu na lu -- čin -- ce,
	smut -- ně so -- bě na -- ří -- ka -- lo po ma -- min -- ce.
\repeat volta 2{
	Že jí br -- zy zem -- řé -- la, žá -- dných pe -- něz ne -- mě -- la,
	že ji bo -- ha -- tý sy -- ne -- ček žá -- dný nech -- ce.
}}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
    b2
    c2:m
    b4 f4
    b2
    f2
    g2:m
    c2
    f2
    b2
    b4 es4
    es2
    es4 b4
    b2
    c2:m
    b4 f4
    b2
}

MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key b \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \transpose b d' \MvmntI}
