% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI = {
  \repeat volta 2{
         c''8 c'' f'' f'' |
         e''4 c'' |
         c''8 d'' b' g' |
         a'4 f' } %5
         \repeat volta 2{a'8 a' a' a' |
         c'' c'' b' b' |
         b'  b' g' b' |
         a'4 f' }}

MvmntIVoiceILyricsVerseI = \lyricmode { 
	\repeat volta 2{Od Rad -- hoš -- tě sví -- tá, ma -- lá chvil -- ka do dně.}
	\repeat volta 2{Poď, má ze všech nej -- mi -- lej -- ší, poď a vy -- pro -- voď ně.}
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
  f2
  c2
  c2
  f2
  f2
  f4
  c4
  c2
  f2
}

MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key f \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \transpose f d \MvmntI}
