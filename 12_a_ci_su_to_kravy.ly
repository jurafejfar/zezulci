% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI =  {
         d'8 fis' a' a' |
         a'4 d'' |
         d''8 cis'' <h' \parenthesize c''> a' |
         h'4 g' |
%5
         g'8 <\parenthesize a' h'> c'' c'' |
         c''4 h' |
         a'2 |
\repeat volta 2{
         a'8 h' c'' c'' |
         h'4 g' |
%10
         a'8 g' fis' e' |
         d'4 c' |
         c'8 d' e' fis' |
         g'4 a' |
         d'2   |}
}

MvmntIVoiceILyricsVerseI = \lyricmode { 
	A čí sú to krá -- vy v_tem ze -- ľe -- ném há -- ji, v_tej ze -- ľe -- néj bře -- zi -- ně?
	\repeat volta 2{Ach mo -- je sú, mo -- je, co sem na -- cho -- va -- la, za -- kéľ si býl na voj -- ně.}
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
  d2
  d2
  d2
  g2
  g4 c4
  c4 g4
  d2
  d2
  g2
  d2
  d4 c4
  c2
  c4 a4
  d2
}

MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key g \major
MvmntIVoiceIClef = \clef treble
MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \MvmntI}
