% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI =  {
\repeat volta 2 {
        e''8 e''4 e''8 |
         f'' e'' d'' c'' |
         e''4 d''8 d'' |
         c'' h' a'4 |
         g'2} 
\repeat volta 2 {
         d''4 d''8 g' |
         c'' d'' e'' e'' |
         e''4 c''8 c'' |
         d'' c'' h'4 |
         a'2  }
}

MvmntIVoiceILyricsVerseI = \lyricmode { 
	\repeat volta 2 {A já sem ne -- byl do -- ma, a -- ni sem ne -- no -- co -- val.}
	\repeat volta 2 {Žen -- ku mně my -- ši zed -- ly, a už sem do -- gaz -- do -- val.}
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
 c2
 c2
 c4 d4
 d2:7
 g2
 g2
 c2
 a2:m
 e2
 a2:m
}

MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key c \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \MvmntI}
