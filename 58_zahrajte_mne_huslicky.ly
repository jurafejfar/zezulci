% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI = {
\repeat volta 2{
         g'8 a' h' a' |
         g'4 h'8 g' |
}
\repeat volta 2{
         h'4 d''8 d'' |
         d''4 c''8 h' |
         a'2 |
}\alternative {{
         a'4 a'8 h' |
         c''4 h'8 a' |
         g'2 |
}{
         a'4 g'8 a' |
         c''4 h'8 a' |
         g'2 
}}
\bar "|."
}

MvmntIVoiceILyricsVerse = \lyricmode {
	Za -- hraj -- te mně hus -- lič -- ky,
}

MvmntIVoiceILyricsVerseI = \lyricmode {
	z_ja -- vo -- ro -- véj deš -- tič -- ky.
\repeat volta 2{
	Já mu -- sím do -- mů jí -- ti,
}\alternative {{
	už ne -- mám na co pí -- ti.
}{
	už ne -- mám na co pí -- ti.
}}
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
    g2
    g2
    g2
    g2
    d2
    d2
    d2
    g2
    d2
    d2
    g2
}


MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key g \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << 
	\new Voice = "melody" { \MvmntIVoiceIMusic } 
	\new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerse } 
	\new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } 
>>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \MvmntI}
