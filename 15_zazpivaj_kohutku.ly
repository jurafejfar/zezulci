% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI =  {
         c''4 c''8 c'' |
         d''4 d''8 d'' |
         e''4 e''8 e'' |
         f''4 d'' |
%5
         c''2 |
         a'4 a'8 a' |
         b'4 d''8 d'' |
         c''4 c''8 b' |
         a'4 g' |
%10
         f'2 |

         g'4 g'8 a' |
         b'4 d''8 d'' |
         c''4 c''8 b' |
         a'4 g' |
         f'2 \bar "|."
}

MvmntIVoiceILyricsVerseI = \lyricmode { 
	Za -- zpí -- vaj ko -- hút -- ku, já ti dám je -- čme -- ňa,
	a -- bych já ne -- za -- spal, jak sem za -- spál vče -- ra,
	a -- bych já ne -- za -- spal, jak sem za -- spál vče -- ra.
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
    f2
    b2
    c2
    f4 b4
    f2
    f2
    b2
    c2
    f4 c4 f2
    c2
    b2
    c2
    f4 c4 f2
}

MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key f \major
MvmntIVoiceIClef = \clef treble
MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \transpose f d \MvmntI}
