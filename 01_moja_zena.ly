% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI =  {
         g'8 a' b'4 |
         a'2 |
         g'8 a' b'4 |
         a'2 |
%5
         b'8 c'' d'' d'' |
         e''4 d'' |
         c''8 b' a' g' |
         c''4 b' |
         a'8 g' fis'4 |
%10
         g'2 \bar "|."
}

MvmntIVoiceILyricsVerseI = \lyricmode {
	Mo -- ja že -- na, mo -- ja že -- na, 
	pro -- pi -- la mně vol -- ky 
	s_in -- ši -- mi pa -- chol -- ky 
	a -- ji ko -- ňa.
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
 g2:m
 d2
 g2:m
 d2
 b2
 c4 b4
 c2:m
 c4:m g4:m
 d2
 g2:m 
}

MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key g \minor
MvmntIVoiceIClef = \clef treble
MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \MvmntI}
