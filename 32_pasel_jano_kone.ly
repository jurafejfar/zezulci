% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI = {
         h'4 a' |
         g' fis' |
         a'2 |
         e' |
%5
         a'4 g' |
         fis' c' |
         e'2 |
         d' |
\repeat volta 2 { d'4 d'8 d' |
%10
         d' h d'4 |
         e' e'8 e' |
         e' c' e'4 |
         fis' d' |
         e' fis' |
%15 
}\alternative {
        {g'4( a') | h'2 }
        {g'2 | g'}
}
\bar "|."
}

MvmntIVoiceILyricsVerseI = \lyricmode { 
	Pá -- sel Ja -- no ko -- ně v_hlu -- bo -- kej do -- li -- ně. 
	\repeat volta 2 {Koň se mu za -- to -- čel, koň se mu za -- to -- čel 
	mi -- lá dí -- vaj}\alternative {{na mě,} {na mě.}}
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
%chord symbols follow
 g2
 g2
 a2
 a2
 d2:7
 d2:7
 c2
 g2
 
 g2
 g2
 c2
 a2:m
 d2
 d2
 g4 d4 
 g2
 \set chordChanges = ##f
 g2
 \set chordChanges = ##t
 g2 
}

MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key g \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \transpose g c' \MvmntI}
