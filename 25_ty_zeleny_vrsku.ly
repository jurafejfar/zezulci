% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI = {
  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
         g'8 a' h'4 c'' |
         d'' h'2 |
         h'8( c'') d''4 d'' |
         e''8 d'' cis''4 d'' |
%5
         e''8 e'' d''4 c''4  |
         h'4 a'2 |
         a'8( h') a'4 g' |
         fis'8 g' a'( h') c''( d'') |
         e'' e'' d''4 c''4 |
%10
         h'4 a'2 |
         a'4 a' h'8( d'') |
         cis'' h' a'4 g' \bar "|."
}

MvmntIVoiceILyricsVerseI = \lyricmode {
	Ty ze -- ľe -- ný vŕ -- šku ty si mně na zá -- va -- dě,
	že ne -- vi -- dím cho -- diť svú mi -- lú po za -- hra -- dě,
	že ne -- vi -- dím cho -- diť svú mi -- lú po za -- hra -- dě.
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
  g2 d4
  g2.
  g2.
  a2 d4
  c2.
  g4 d2
  d2 g4
  d2.
  c2.
  g4 d2
  d2.
  a4 d4 g4
}



MvmntIVoiceITimeSig = \time 3/4 
MvmntIVoiceIKeySig = \key g \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \transpose g f \MvmntI}
