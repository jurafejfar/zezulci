% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI =  {
    \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)

          a'4 cis''8 h' a' fis' |
         e' e' d''4 cis'' |
         h'8 h'4 a'8 gis' fis' |
         a' gis'8 fis'4 e' |
%5
         a'8 a' gis' fis' e'4 |
         a'8 a' h' d'' cis''4 |
         e''8 d''8 cis''4 h'4 |
         a'2. \bar "|."
}

MvmntIVoiceILyricsVerseI = \lyricmode { 
	Aj u sú -- se -- dů na ko -- peč -- ku dos -- tal sem jed -- nu pro dě -- več -- ku, 
	a -- ľe né hru -- bě, je -- nom po hu -- bě, dos -- tal jsem fac -- ku.
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
 a2.
 a4  d4  a4
 e2.
 e4  h4  e4
 a4 e2 
 a4 e4 a4
 h2:m e4
 a2.
}

MvmntIVoiceITimeSig = \time 3/4 
MvmntIVoiceIKeySig = \key a \major
MvmntIVoiceIClef = \clef treble
MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \MvmntI}
