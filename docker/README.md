create docker image

using packer

```bash
packer build -only=docker debian-lilypond.json
```

or using docker

```bash
docker build -t registry.gitlab.com/jurafejfar/zezulci/debian-lilypond:latest .
docker push registry.gitlab.com/jurafejfar/zezulci/debian-lilypond:latest
```
