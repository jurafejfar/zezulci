% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI = {
         c'8 d' e' e' |
         e' f' g' g' |
         c''4 e'8 g' |
         a'4 f' |
%5
         g' f'8 e' |
         d'2 |
         \repeat volta 2{
         e'4 e'8 g' |
         a'4 f' |
         g' e'8 d' |
%10
         c'2  }
}

MvmntIVoiceILyricsVerseI = \lyricmode { 
	Vy haj -- ní -- ci, ho -- ry haj -- te,
	aj vy haj -- ní -- ci, ho -- ry haj -- te
	 \repeat volta 2{ a ci -- zím že -- nám po -- koj daj -- te.} 
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
 c2
 c2
 c2
 f2
 c2
 g2
 c2
 f2
 c4 g4
 c2
}

MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key c \major
MvmntIVoiceIClef = \clef treble
MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \transpose c' d' \MvmntI}
