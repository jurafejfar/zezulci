% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI = {
  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
         e''8 g'' fis'' g'' fis'' e'' |
         d''2.  |
         c''8 g' c'' d'' e'' fis'' |
         g''2. |
%5
         a''8 a'' a'' g'' fis'' e'' |
         d''2. |
         c''8 d'' e'' d'' e'' h' |
         c''2. \bar "|."
}

MvmntIVoiceILyricsVerseI = \lyricmode {
	Vím já jed -- no o -- kyn -- ko,
	vím já jed -- no o -- kyn -- ko,
	a gde spá -- váš, Ma -- ryn -- ko,
	a gde spá -- váš, Ma -- ryn -- ko.
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
 c2.
 g2.
 c2. 
 c2.
 d2.
 g2.
 c2  g4 
 c4
}

MvmntIVoiceITimeSig = \time 3/4 
MvmntIVoiceIKeySig = \key c \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \transpose c' f \MvmntI}
