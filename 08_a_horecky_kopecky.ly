% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI =  {
\repeat volta 2 {
         g'8 a' h' c'' |
         d''4 d''8 d'' |
         <\parenthesize h' d''> c'' <h' \parenthesize d''>4 |
         <g' \parenthesize h'> <fis' \parenthesize a'> |
%5
         d'2   |
         }
\repeat volta 2 {
         d''4 c''8 c'' |
         h'4 a'8 g' |
         a' h' c''4 |
         <h' \parenthesize d''> <a' \parenthesize c''> |
%10
         <g' \parenthesize h'>2
}
}

MvmntIVoiceILyricsVerseI = \lyricmode { 
	\repeat volta 2 {A ho -- reč -- ky, ko -- peč -- ky ze -- ľe -- ňaj -- te sa mně,}
	\repeat volta 2 {bys -- třic -- ké děv -- ča -- ta ne -- ra -- duj -- te sa mně.}
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
 g2
 g2
 g2
 g4 a4
 d2
 g4 d4
 g2
 d2
 g4 d4
 g2
}

MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key g \major
MvmntIVoiceIClef = \clef treble
MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \MvmntI}
