% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI = {
  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
\repeat volta 2 {
        e'8 e' e' fis' gis' a' |
        h' h' h' cis'' dis'' h' |
        cis'' dis'' e''4 e'' |
        dis''8 cis'' h'2  
}\repeat volta 2 {
        a'8 cis'' cis'' cis'' h' a' |
        gis' h' h' h' a' gis' |
        fis' gis' a'4 fis' |
}\alternative{{
	e'8 gis' h'2
}{
	gis'8 fis' e'2 \bar "|."
}}
}

MvmntIVoiceILyricsVerseI = \lyricmode {
\repeat volta 2 {
	Te -- če vo -- da sko -- kem,
	pod -- ľa na -- šich o -- ken,
	ne -- mó -- žu ju zas -- ta -- vit.}
\repeat volta 2 {
	Roz -- hně -- va -- la sem si
	svo -- je -- ho sy -- neč -- ka,
	ne -- mó -- žu ho 
}\alternative{{
	u -- dob -- řiť
}{
	u -- dob -- řiť.
}}
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
    e2.
    e2.
    a2.
    e2.
    a2.
    e2.
    h2.
    e2.
  \set chordChanges = ##f
    e2.
}



MvmntIVoiceITimeSig = \time 3/4 
MvmntIVoiceIKeySig = \key e \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \transpose e d \MvmntI}
