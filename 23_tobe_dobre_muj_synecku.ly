% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI =  {
         a'8 d'' cis'' h' |
         a' a' h' cis'' |
         d''4 e'' |
         a'2 |
%5
         a'8 d'' cis'' h' |
         a' h' a' g' |
         fis'4 e' |
         d'2 |
         \repeat volta 2 { fis'4. d'8 |
%10
         e'4 d' |
         fis'8 a' gis' a' |
         h'4 a' |
         e'2 |
         a'8 d'' cis'' h' |
%15
         a' h' a' g' |
         fis'4 e' |
         d'2  }}

MvmntIVoiceILyricsVerseI = \lyricmode { 
	To -- bě dob -- ře, můj sy -- neč -- ku, a mně zle.
	To -- bě líč -- ka roz -- kvé -- ta -- jú a mně ne.
	\repeat volta 2 {To -- bě ros -- te za klo -- búč -- kem rů -- žič -- ka,
	já mám stá -- ľe u -- pla -- ka -- né o -- čič -- ka.}
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
  d2
  d2
  d4 e4
  a2
  d2
  d2
  d4 a4
  d2
  d2
  a4 d4
  d2
  g4 d4
  a2
  d2
  d2
  d4 a4
  d2
}

MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key d \major
MvmntIVoiceIClef = \clef treble
MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \MvmntI}
