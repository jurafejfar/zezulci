% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI = {
         e'8 gis' h' h' |
         h' cis'' h' a' |
         gis' a' h' e'' |
         a'2 |
%5
         gis'8 a' h' h' |
         h' cis'' h' a' |
         gis' fis' e' fis' |
         d'2 |

         a'4 gis'8 fis' |
%10
         e' e' d'4 |
         a' gis'8 fis' |
         e' fis' d'4 |
         
         a'8 a' a' a' |
         h' c'' h' a' |
%15
         gis' gis' fis' gis' |
         e'2 \bar "|."
}

MvmntIVoiceILyricsVerseI = \lyricmode {
	Dy -- by ně dal pán Bůh dos -- tať děv -- če ta -- ko -- vé,
	co by o -- no mě -- lo šty -- ry ctnos -- ti při so -- bě.
	Ši -- kov -- né a mrav -- né, ctnost -- né, bo -- ha -- boj -- né,
	vi -- děl sem tam se -- dm ti -- síc u ňho na sto -- ľe.
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
    e2
    e2
    e2
    a2
    e2
    e2
    e2
    d2
    d2
    a4 d4
    d2
    a4 d4
    a2:m
    a2:m
    h2
    e2
}


MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key e \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \transpose e d \MvmntI}
