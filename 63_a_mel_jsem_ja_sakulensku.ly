% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI =  {
         g'8 h' d'' d'' |
         d'' e'' d'' c'' |
         h' d'' g'4 | \break
         c''8 c'' c'' c'' |
%5
         c'' d'' c'' b' |
         a' g' f'4 | \break
         f'8 f' f' f' |
         d'8 e' c'4 |
         f'8 f' f' f' |
%10
         d' e' c'4 |

         c''8 c'' c'' c'' |
         c'' e'' d'' c'' |
         h' h' a' a' |
         g'2 \bar "|."
}

MvmntIVoiceILyricsVerseI = \lyricmode {
	A měl sem já sa -- ku -- ľen -- skú ko -- by -- lu, 
	a já na ní za svú mi -- lú po -- je -- du.
	Sa -- ku -- ľen -- ská ko -- by -- la, tak o -- na mňa no -- si -- la,
	u sú -- se -- dů do hno -- jův -- ky tam mňa sho -- di -- la.
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
    g2
    g2
    g2
    c2
    c2
    f2
    f2
    g4 c4
    f2
    g4 c4
    c2
    c2
    d2
    g2
}


MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key g \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \MvmntI}
