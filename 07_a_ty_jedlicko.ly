% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI =  {
          a'4 a'8 h' |
         a'4 g' |
         a'8 h' c''4 |
         e'' c''8 c'' |
%5
         d''4 g' |
         a'8 h' c''4 |
         e''8 d'' c'' h' |
         a'4 gis'8 a' |
         c'' h' c'' h' |
%10
         a'4 gis' |
         a'2 \bar "|."
}

MvmntIVoiceILyricsVerseI = \lyricmode { 
	Aj ty jed -- lič -- ko pod -- ťa -- tá, 
	aj ty jed -- lič -- ko pod -- ťa -- tá, 
	ško -- da tvé -- ho vy -- da -- ťá, 
	ško -- da tvé -- ho vy -- da -- ťá. 
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
 a2:m
 a4:m g4
 g4 c4
 c2
 g2
 g4 c4
 a2:m
 a4:m e8 a8:m
 a2:m
 a4:m e4
 a2:m
}

MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key a \minor
MvmntIVoiceIClef = \clef treble
MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \MvmntI}
