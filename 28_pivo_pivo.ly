% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI =  {
         g'8 a' h' h' |
         h' c'' d'' d'' |
         d''4 h'8 d'' |
         e''4 c'' |
%5
         d'' c''8 h' |
         a'2 |
         h'4 h'8 d'' |
         d''4 c'' |
         d'' h'8 a' |
%10
         g'2 \bar "|."
}

MvmntIVoiceILyricsVerseI = \lyricmode { 
	Pi -- vo, pi -- vo, pi -- vo mo -- je, 
	ej pi -- vo, pi -- vo, pi -- vo mo -- je 
	ej zeh -- na -- los ňa z_mo -- jí ro -- ľe. 
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
  g2
  g2
  g2
  c2
  g2
  d2
  g2
  g4 c4
  g4 d4
  g2
}

MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key g \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \MvmntI}
