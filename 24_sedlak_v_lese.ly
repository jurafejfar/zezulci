% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI =  {
         g'8 f' e' f' |
         g' a' h' g' |
         g' f' e' f' |
         g' a' h'4 |
%5
         g'2 | \break
         g'8 c'' c'' c'' |
         c'' d'' e'' c'' |
         d'' c'' h' a' |
         g' a' h'4 |
%10
         g'2 \bar "|."
}

MvmntIVoiceILyricsVerseI = \lyricmode {
	Sed -- lák v_ľe -- se buč -- ka ře -- že,
	haj -- ný k_ně -- mu s_taš -- kú ľe -- ze.
	Poč -- kaj ty, ľes -- ní zboj -- ní -- ku, 
	bu -- deš se -- děť v_ka -- mr -- lí -- ku.
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
  c2
  c4 g4
  c2
  c4 g4
  g2
  c2
  c2
  d2
  g2
  g2
}

MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key g \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \MvmntI}
