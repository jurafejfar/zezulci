% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI =  {
         g'8 a' h' a' |
         g' a' d'4 |
         d'8 e' f'? f' |
         e' d' c'4 |
%5
         c'' c''8 c''16( d'') |
         e''4. c''8 |
         h'4 a' |
         gis'8 a' h' a' |
         gis' a' d'4 |
%10
         d'8 e' f'? f' |
         e' d' c'4 |
         g' g'8 g' |
         a'4. fis'8 |
         e'4 d' \bar "|."
}

MvmntIVoiceILyricsVerseI = \lyricmode {
	Za -- bo -- lé -- la mňa hla -- va 
	za -- bo -- lé -- la mňa hla -- va 
	od ve -- če -- ra do rá -- na 
	a v_pon -- dě -- lí ce -- lý deň 
	a v_pon -- dě -- lí ce -- lý deň 
	a ce -- lý bo -- ží tý -- deň.
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
 g2
 a4 d4
 d2:m
 g4 c4
 c2
%5
 a2:m
 e4 a4:m
 e2
 e4 d4
%10
 d2:m
 g4 c4
 g2:m
 d2
 a4 d4 
}

MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key d \minor
MvmntIVoiceIClef = \clef treble
MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \MvmntI}
