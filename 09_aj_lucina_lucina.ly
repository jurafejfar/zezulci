% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI =  {
         g'8 a' h' c'' |
         d''4 d''8 e'' |
         d'' cis'' d''4 |
         g'8 a' h' c'' |
%5
         d''4 d''8 e'' |
         d'' cis'' d''4 |
         
         \repeat volta 2{
         a'4 g'8 a' |
         d'2 |
         d''4 c''8 h' |
%10
         a'2 |
         a'4 g'8 a' |
         d'2 |
         c''4 h'8 a' |
         g'2   |}}

MvmntIVoiceILyricsVerseI = \lyricmode { 
	Aj lu -- či -- na, lu -- či -- na ze -- ľe -- ná, poď, má mi -- lá roz -- mi -- lá, poď za mňa.
	\repeat volta 2{Proč bych ne -- šla, šak sem švar -- ná, ne -- bu -- de ťa za mňa haň -- ba.}
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
 g2
 g2
 a4 g4 
 g2
 g2
 a4 g4 
 d2
 d2
 d4. g8
 d2 
 d2 
 d2 
 c4 d4
 g2
}

MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key g \major
MvmntIVoiceIClef = \clef treble
MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \MvmntI}
