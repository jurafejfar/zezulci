% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI = {
  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
\repeat volta 2 {
         b'8 b' b' c'' d''8 es''8 
         f''4 f''2
         g''8 g''4 \autoBeamOff a''8 \autoBeamOn b''8 a''8
         g''4 f''2 
         } \repeat volta 2 {
         es''8 es'' es'' f''  g'' es'' 
         d''4 c''2
         c''8 c''4 \autoBeamOff f''8 \autoBeamOn es'' d'' 
         c''4 b'2}
}

MvmntIVoiceILyricsVerseI = \lyricmode {
	\repeat volta 2 {
		Ne -- ní kaž -- dé děv -- če věr -- né,
		ke -- ré má o -- či čer -- né.
	}\repeat volta 2 {
		Ne -- ní kaž -- dé děv -- ča lži -- vé,
		ke -- ré má oč -- ka si -- vé.
	}
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
    b2.
    b2.
    es2.
    es4 b2
    es2.
    b4 f2
    f2.
    f4 b2
}

MvmntIVoiceITimeSig = \time 3/4
MvmntIVoiceIKeySig = \key b \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \transpose b f \MvmntI}
