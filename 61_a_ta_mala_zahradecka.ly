% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI = {
\repeat volta 2{
         g'8 g' g' g' |
         a' g' fis' d' |
         c' d' e' f' |
         d'2 }
\repeat volta 2{
         c'8 h c' d' |
         f' e' d'4 |
         c'8 h c' d' |
         f' e' d'4 |

         g'8 g' g' g' |
%10
         a' g' fis' d' |
         e' d' c' h |
         c'2 }
}

MvmntIVoiceILyricsVerse = \lyricmode {
	A ta ma -- lá za -- hrá -- deč -- ka tr -- ním ple -- te -- ná,
}

MvmntIVoiceILyricsVerseI = \lyricmode {
	a v_ní ros -- te trá -- _ -- věn -- ka pěk -- ná ze -- ľe -- ná.
\repeat volta 2{
	Je v_ní pěk -- ný roz -- ma -- rýn a já o něm dob -- ře vím,
	on má pěk -- ný bí -- lý vŕ -- šek, já ho vy -- lo -- mím.
}}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
    c2
    d2
    c2
    g2
    c2
    g2
    c2
    g2
    c2
    d2
    c4 g4
    c2
}

MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key c \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << 
	\new Voice = "melody" { \MvmntIVoiceIMusic } 
	\new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerse } 
	\new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } 
>>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \transpose c g \MvmntI}
