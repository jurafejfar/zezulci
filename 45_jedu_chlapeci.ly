% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI = {
         g''4 e''8 c'' |
         d''4 g' |
         d''8 d''4 e''8 |
         fis''4 g'' |
%5
         g'' e''8 c'' |
         d''4 g' |
         d''8 d''4 c''8 |
         h'4 c'' \bar "|."
}

MvmntIVoiceILyricsVerseI = \lyricmode {
	Je -- dú chla -- pe -- ci ho -- re Hu -- lí -- nem,
	ve -- zú si děv -- ča pod roz -- ma -- rý -- nem.
}

MvmntIVoiceIChords =  \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
    c2
    g2
    g2
    d4 g4
    c2
    g2
    g2
    g4 c4
}

MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key c \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \transpose c' g \MvmntI}
