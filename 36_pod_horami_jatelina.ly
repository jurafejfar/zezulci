% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI = {
         h'8 h' ais' fis' |
         gis' e' fis'4 | e'2
         h'8 h' ais' fis' |
         gis' e' fis'4 | e'2
  
         gis'4. fis'8 |
         gis'4. fis'8 |
         gis' a' cis''4 |
         h'2 |
         gis'8 ais' h' fis' |
         gis' e' fis'4 |
         e'2 \bar "|."
}

MvmntIVoiceILyricsVerseI = \lyricmode { 
	Pod ho -- ra -- mi ja -- te -- li -- na,
	ža -- la ju tam Ka -- te -- ri -- na.
	Ža -- la, ža -- la, na -- ža -- la si, 
	po -- tka -- li ju tre ju -- ha -- si.
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
 e4 fis4
 e4 h4
 e2
 e4 fis4
 e4 h4
 e2
 e4. h8
 e4. h8
 e4 a4
 e2
 e4. fis8
 e4 h4
 e2
}

MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key e \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \transpose e g \MvmntI}
