% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI =  {
         g'8 h' d'' d'' |
         d'' e'' d''4 |
         g'2 |
         c''8 c'' c'' c'' |
%5
         c'' b' a'4 |
         b'2 | 
         \repeat volta 2{ \time 3/4 g'4 d'2 |
         g'4 a'2  |
         \time 2/4 b'8 a' b' c'' |
%10
         d''4 d'' |
         d''8 e'' d'' c'' |
         d'' b' a' g' |
         fis'4 g'}}

MvmntIVoiceILyricsVerseI = \lyricmode { 
		Dyž sem bý -- val mlá -- de -- neč -- kem,
		cho dil sem si pod pé -- reč -- kem.
		\repeat volta 2{Cho -- dil sem si po ze -- ľe -- néj lú -- ce, 
		no -- síl sem si o -- bu -- še -- ček "v ru" -- ce.}
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
 g2
 g2
 g2
 c2
 f2
 b2
 g4:m d2 
 g4:m d2 
 g2:m
 g2:m
 c2
 g2:m
 d4 g4:m
}

MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key g \major
MvmntIVoiceIClef = \clef treble
MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \MvmntI}
