% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI = {
         h'8 c'' d''4 d'' |
         e''8 d'' cis''4 d'' |
         h'8 c'' d''4 d'' |
         e''8 d'' cis''4 d'' |
%5
         d''8 c''? h'4 g' |
         a' d'2 |
         a'8 h' c''2 |
         d''8 c'' h'4 g' |
         a' d'2 |
%10
         h'8 a' g'2 \bar "|."
}

MvmntIVoiceILyricsVerseI = \lyricmode { 
	V_ši -- rém po -- li hruš -- ka sto -- jí, o -- lá -- ma -- ná až do po -- ly.
	A gdo ju o -- lá -- mal? O -- ga -- ři pe -- čá -- cí ze -- mňá -- ky k_ve -- če -- ři.
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
 g2.
 a2 d4
 g2.
 a2 d4
 g2.
 d2.
 d2.:7
 g2.
 d2.
 d4:7
 g2
}


MvmntIVoiceITimeSig = \time 3/4 
MvmntIVoiceIKeySig = \key g \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \MvmntI}
