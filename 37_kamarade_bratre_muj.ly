% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI = {
         e'8 fis' gis'4 fis' |
         e'8 dis' e'2 |
         gis'8 ais' h'4 h' |
         ais'8 gis' fis'2 |
%5
\repeat volta 2{
        fis'8 ais' h'4 h' |
         cis''8 h' ais'4 fis' |
         e'8 fis' gis'4 e' |
         dis'8 fis' e'2 
}}

MvmntIVoiceILyricsVerseI = \lyricmode {
	Ka -- ma -- rá -- de, bra -- tře můj, já to -- bě ne -- co po -- vím,\\
	\repeat volta 2{že ty za mú mi -- lú cho -- díš, a já za tvú ne -- cho -- dím.}
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
    e2 h4
    e8 h8 e2
    e2.
    fis4 h2
    fis4 h2
    fis2.
    e2.
    h4 e2
}

MvmntIVoiceITimeSig = \time 3/4 
MvmntIVoiceIKeySig = \key e \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \transpose e g \MvmntI}
