% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI = {
         fis'4 fis'8 fis' |
         fis'4 d' |
         a'8 gis' fis'4 |
         e'4 e'8 e' |
%5
         e'4 a |
         d'8 e' fis'4 |
         fis'8 e' d' cis' |
         h4 ais8 h |
         d' e' d' cis'  |
%10
         h4 ais  |
         h2 \bar "|."
}

MvmntIVoiceILyricsVerseI = \lyricmode { 
	Gde si, sy -- neč -- ku, gde si býl,
	gde si, sy -- neč -- ku, gde si býl,
	žes ko -- níč -- ka vy -- bro -- dil,
	žes ko -- níč -- ka vy -- bro -- dil?
}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
 d2
 d2
 e4 d4 
 a2
 a2
 d2 
 h4.:m fis8 
 h4:m fis8 h8:m
 h4.:m fis8
 h4:m fis4
 h2:m 
}

MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key h \minor
MvmntIVoiceIClef = \clef treble
MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << \new Voice = "melody" { \MvmntIVoiceIMusic } \new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } >>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \transpose h e' \MvmntI}
