% Copyright Jiří Fejfar 2008, 2018
%
% Na toto dílo se vztahuje licence EUPL (dále jen „licence“).
% Toto dílo smíte používat pouze v souladu s licencí.
% Kopii licence najdete na adrese:
%
% https://joinup.ec.europa.eu/software/page/eupl
%
% Pokud to nevyžaduje rozhodné právo nebo není písemně dohodnuto jinak, je software šířený s touto licencí šířen „TAK, JAK JE“, tedy
% BEZ JAKÝCHKOLI ZÁRUK A PODMÍNEK, ať výslovných nebo implicitních.
% Ustanovení upravující povolení a omezení v rámci licence najdete v konkrétním jazykovém znění licence.

\version "2.18"
\language "deutsch"

% The music follows

MvmntIVoiceI =  {
\repeat volta 2{
         g'8 a' h' g' |
         fis' e' fis' d' |
         e'4 fis' |
         g'2  
}\repeat volta 2{
         h'8 h' h' a' |
         g' fis' e'4  |
	 a'8 a' a' g' |
         fis' e' d'4 |
         e' fis' |
         g'2}
}

MvmntIVoiceILyricsVerse = \lyricmode {
	Po -- chvá -- ľen buď Je -- žíš Kris -- tus, už k_Vám jdu.
}

MvmntIVoiceILyricsVerseI = \lyricmode {
\repeat volta 2{
 	Dá -- te -- li mně va -- šú dcé -- ru o -- prav -- du?
}\repeat volta 2{
	A jak mně ju ne -- dá -- te
	a tak si ju ne -- chá -- te,
	já pryč jdu. 
}}

MvmntIVoiceIChords = \new ChordNames \chordmode {
\germanChords
\set chordNameLowercaseMinor = ##t
\set chordChanges = ##t
%chord symbols follow
    g2
    d2
    c4 d4
    g2
    g2
    e2:m
    a2
    d2
    c4 d4
    g2
}


MvmntIVoiceITimeSig = \time 2/4 
MvmntIVoiceIKeySig = \key g \major
MvmntIVoiceIClef = \clef treble

MvmntIVoiceIProlog = { \MvmntIVoiceITimeSig \MvmntIVoiceIKeySig \MvmntIVoiceIClef}
MvmntIVoiceIMusic = {\MvmntIVoiceIProlog \MvmntIVoiceI}
MvmntIStaffI = \new Staff << 
	\new Voice = "melody" { \MvmntIVoiceIMusic } 
	\new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerse } 
	\new Lyrics \lyricsto "melody" { \MvmntIVoiceILyricsVerseI } 
>>
MvmntI = << \MvmntIVoiceIChords \MvmntIStaffI >>
\score { \MvmntI}
